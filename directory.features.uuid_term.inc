<?php
/**
 * @file
 * directory.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function directory_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Online Aquarium Shop',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '36393cdb-a77f-4935-8269-f9b5ac874d57',
    'vocabulary_machine_name' => 'directory_category',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Pet Lodging',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '5312f781-8e4e-42df-a985-890f2666329b',
    'vocabulary_machine_name' => 'directory_category',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Pet Shop',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '5816a295-9e47-477b-8299-f63b0ac61b34',
    'vocabulary_machine_name' => 'directory_category',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Aquarium',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 3,
    'uuid' => '73e2a3c4-4189-4c3d-95fb-b084b3327e35',
    'vocabulary_machine_name' => 'directory_category',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Pet Grooming',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '7f2299b5-c0c6-4f21-addb-bc013d2bd393',
    'vocabulary_machine_name' => 'directory_category',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'NGO',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '814d0470-3760-4caa-b719-414e6d504efc',
    'vocabulary_machine_name' => 'directory_category',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Vet',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '91d42278-b8df-4f40-abf5-fb33f34a84eb',
    'vocabulary_machine_name' => 'directory_category',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Online Pet Shop',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'a27b84a7-42ea-449d-84aa-bceecaf812bf',
    'vocabulary_machine_name' => 'directory_category',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Pet Training',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'da0039e4-f96a-4fd0-bc95-23e4f20c99d2',
    'vocabulary_machine_name' => 'directory_category',
    'metatags' => array(),
  );
  return $terms;
}
