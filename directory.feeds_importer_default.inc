<?php
/**
 * @file
 * directory.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function directory_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'directory_importer';
  $feeds_importer->config = array(
    'name' => 'Directory importer',
    'description' => 'Import from csv file of directory entries',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'csv',
        'direct' => 0,
        'directory' => 'public://feeds',
        'allowed_schemes' => array(
          'public' => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '3',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'Name of business',
            'target' => 'field_directory_businessname',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'Directory category',
            'target' => 'field_directory_category',
            'term_search' => '0',
            'autocreate' => 0,
          ),
          2 => array(
            'source' => 'Description',
            'target' => 'field_directory_description',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'Address',
            'target' => 'field_directory_address:thoroughfare',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'City/Town',
            'target' => 'field_directory_address:locality',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'State',
            'target' => 'field_directory_address:administrative_area',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'Country',
            'target' => 'field_directory_address:country',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'PIN Code',
            'target' => 'field_directory_address:postal_code',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'Email',
            'target' => 'field_directory_email',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'Phone',
            'target' => 'field_directory_phone',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'Website',
            'target' => 'field_directory_url_link:url',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => 'Serial',
            'target' => 'guid',
            'unique' => 1,
          ),
          12 => array(
            'source' => 'Serial',
            'target' => 'field_directory_serial',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'directory_entry',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['directory_importer'] = $feeds_importer;

  return $export;
}
