build2014092101
- extra image field deleted

build2014091802
- teasers view - global text area - see all products changed to see all

build2014091801
- made title 'directory' in teasers view
- gave it class=back, h4, strong

build2014091502
- make comment form visible no ajax
- change download pdf to upload brochure

build2014091501 
- directory default image of 185
- taxonomy terms included
- teaser image style 60x60

7.x-1.0-dev1
- initial commit on sep 12 2014
