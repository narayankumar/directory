<?php
/**
 * @file
 * directory.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function directory_default_rules_configuration() {
  $items = array();
  $items['rules_actions_on_publishing_directory'] = entity_import('rules_config', '{ "rules_actions_on_publishing_directory" : {
      "LABEL" : "Actions on publishing Directory",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_update--directory_entry" : { "bundle" : "directory_entry" } },
      "IF" : [
        { "OR" : [
            { "component_rules_evaluate_if_directory_published_and_author_not_editor" : { "node" : [ "node-unchanged" ], "node_updated" : [ "node" ] } },
            { "component_rules_evaluate_if_directory_ownership_has_changed_from_editor" : { "node" : [ "node-unchanged" ], "node_updated" : [ "node" ] } }
          ]
        }
      ],
      "DO" : [
        { "mail" : {
            "to" : "[node:author:mail]",
            "subject" : "Your directory entry has been published at Gingertail.in",
            "message" : "Hello [node:author]:\\r\\n\\r\\nYour directory entry \\u0027[node:title]\\u0027 has been published at Gingertail.in with you as the owner.\\r\\n\\r\\nYou can view, check and edit the entry at: [node:url]\\r\\n\\r\\n-- Your friendly robot at Gingertail.in",
            "language" : [ "" ]
          }
        },
        { "drupal_message" : { "message" : "A message has been sent to [node:author] intimating publication of [node:title]" } }
      ]
    }
  }');
  $items['rules_evaluate_if_directory_ownership_has_changed_from_editor'] = entity_import('rules_config', '{ "rules_evaluate_if_directory_ownership_has_changed_from_editor" : {
      "LABEL" : "Evaluate if directory ownership has changed from editor",
      "PLUGIN" : "and",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "node" : { "label" : "Directory Unchanged", "type" : "node" },
        "node_updated" : { "label" : "Directory Updated", "type" : "node" }
      },
      "AND" : [
        { "user_has_role" : {
            "account" : [ "node:author" ],
            "roles" : { "value" : { "4" : "4", "5" : "5" } },
            "operation" : "OR"
          }
        },
        { "NOT user_has_role" : {
            "account" : [ "node-updated:author" ],
            "roles" : { "value" : { "4" : "4", "5" : "5" } },
            "operation" : "OR"
          }
        }
      ]
    }
  }');
  $items['rules_evaluate_if_directory_published_and_author_not_editor'] = entity_import('rules_config', '{ "rules_evaluate_if_directory_published_and_author_not_editor" : {
      "LABEL" : "Evaluate if directory published and author not editor",
      "PLUGIN" : "and",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "node" : { "label" : "Directory Unchanged", "type" : "node" },
        "node_updated" : { "label" : "Directory Updated", "type" : "node" }
      },
      "AND" : [
        { "data_is" : { "data" : [ "node:status" ], "value" : "0" } },
        { "data_is" : { "data" : [ "node-updated:status" ], "value" : "1" } },
        { "NOT user_has_role" : {
            "account" : [ "node:author" ],
            "roles" : { "value" : { "4" : "4", "5" : "5" } },
            "operation" : "OR"
          }
        }
      ]
    }
  }');
  $items['rules_im_owner_messaging_for_regd_user'] = entity_import('rules_config', '{ "rules_im_owner_messaging_for_regd_user" : {
      "LABEL" : "I\\u0027m owner messaging for regd user",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "flag" ],
      "ON" : { "flag_flagged_im_owner_for_registered" : [] },
      "IF" : [ { "data_is" : { "data" : [ "flagged-node:author" ], "value" : "3" } } ],
      "DO" : [
        { "redirect" : { "url" : "\\u003Cfront\\u003E" } },
        { "drupal_message" : { "message" : "Your message has been sent to the site editor. Thanks for the participation." } },
        { "mail" : {
            "to" : "editor@gingertail.in",
            "subject" : "I\\u0027m the Owner flag has been set on Directory entry",
            "message" : "Hi:\\r\\n\\r\\nOwnership flag has been set on Directory entry at [flagged-node:url] by [flagging-user:name]\\r\\n\\r\\n[flagging-user:name]\\u0027s message is: \\u0022[flagging:field_owner_flag_message]\\u0022\\r\\n\\r\\nYou can visit the directory entry at: [flagged-node:url]\\r\\n\\r\\nOr you can to write to user: [flagging-user:mail]\\r\\n\\r\\nOr get other details of user by clicking this: [flagging-user:url]\\r\\n\\r\\n-- Your friendly robot at gingertail.com\\r\\n",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_im_owner_redirect_for_anon_user'] = entity_import('rules_config', '{ "rules_im_owner_redirect_for_anon_user" : {
      "LABEL" : "I\\u0027m owner redirect for anon user",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "flag" ],
      "ON" : { "flag_flagged_im_owner_for_anon" : [] },
      "DO" : [ { "redirect" : { "url" : "user\\/login" } } ]
    }
  }');
  $items['rules_incorrect_info_for_regd_user'] = entity_import('rules_config', '{ "rules_incorrect_info_for_regd_user" : {
      "LABEL" : "Incorrect info for regd user",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "flag" ],
      "ON" : { "flag_flagged_authenticated_flag_for_incorrect" : [] },
      "DO" : [
        { "redirect" : { "url" : "\\u003Cfront\\u003E" } },
        { "drupal_message" : { "message" : "Your message has been forwarded to the site editor. Thanks for your participation." } },
        { "mail" : {
            "to" : "editor@gingertail.in",
            "subject" : "Incorrect information flag has been set on directory entry",
            "message" : "Hi:\\r\\n\\r\\nThe Incorrect Information flag has been set on the directory entry at [flagged-node:url] by user [flagging-user:name].\\r\\n\\r\\nAccording to [flagging-user:name], the relevant information is as under:\\r\\n\\r\\n\\u0022[flagging:field_incorrect_info_message]\\u0022\\r\\n\\r\\nYou can visit the directory entry by going to [flagged-node:url] or you can contact [flagging-user:name] at [flagging-user:mail]\\r\\n\\r\\n-- Your friendly robot at gingertail.in",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_test_flag_for_anon'] = entity_import('rules_config', '{ "rules_test_flag_for_anon" : {
      "LABEL" : "Incorrect info for anon user",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "flag" ],
      "ON" : { "flag_flagged_anon_flag_for_incorrect_info" : [] },
      "DO" : [ { "redirect" : { "url" : "user\\/login" } } ]
    }
  }');
  return $items;
}
