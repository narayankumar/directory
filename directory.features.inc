<?php
/**
 * @file
 * directory.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function directory_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function directory_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_fe_nodequeue_export_fields().
 */
function directory_fe_nodequeue_export_fields() {
  $nodequeues = array();

  // Exported nodequeues: ad_block_directory
  $nodequeues['ad_block_directory'] = array(
    'name' => 'ad_block_directory',
    'title' => 'Ad block - Directory',
    'subqueue_title' => '',
    'size' => 8,
    'link' => '',
    'link_remove' => '',
    'owner' => 'nodequeue',
    'show_in_ui' => 1,
    'show_in_tab' => 1,
    'show_in_links' => 0,
    'reference' => 0,
    'reverse' => 0,
    'i18n' => 0,
    'subqueues' => 1,
    'types' => array(
      0 => 'advertisement',
    ),
    'roles' => array(),
    'count' => 0,
  );

  return $nodequeues;
}

/**
 * Implements hook_flag_default_flags().
 */
function directory_flag_default_flags() {
  $flags = array();
  // Exported flag: "Anon flag for incorrect info".
  $flags['anon_flag_for_incorrect_info'] = array(
    'entity_type' => 'node',
    'title' => 'Anon flag for incorrect info',
    'global' => 0,
    'types' => array(
      0 => 'directory_entry',
    ),
    'flag_short' => 'Information is incorrect',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Unflag \'information is incorrect\'',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => 'Already Flagged',
    'link_type' => 'normal',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'teaser' => 'teaser',
      'rss' => 0,
      'search_index' => 0,
      'search_result' => 0,
      'diff_standard' => 0,
      'node_gallery_node_thumbnail' => 0,
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => 0,
    'i18n' => 0,
    'module' => 'directory',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  // Exported flag: "Authenticated flag for incorrect info".
  $flags['authenticated_flag_for_incorrect'] = array(
    'entity_type' => 'node',
    'title' => 'Authenticated flag for incorrect info',
    'global' => 0,
    'types' => array(
      0 => 'directory_entry',
    ),
    'flag_short' => 'Information is incorrect',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Cancel \'information is incorrect\'',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'confirm',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'teaser' => 'teaser',
      'rss' => 0,
      'search_index' => 0,
      'search_result' => 0,
      'diff_standard' => 0,
      'node_gallery_node_thumbnail' => 0,
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => 0,
    'i18n' => 0,
    'flag_confirmation' => 'Are you sure? Give details below.',
    'unflag_confirmation' => 'Are you sure you want to un-flag this content?',
    'module' => 'directory',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  // Exported flag: "I&#039;m the owner for anon".
  $flags['im_owner_for_anon'] = array(
    'entity_type' => 'node',
    'title' => 'I\'m the owner for anon',
    'global' => 0,
    'types' => array(
      0 => 'directory_entry',
    ),
    'flag_short' => 'I\'m the Owner of this product/service',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Unflag this item',
    'unflag_long' => '',
    'unflag_message' => 'Cancel - I\'m not the owner',
    'unflag_denied_text' => 'Already flagged',
    'link_type' => 'normal',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'teaser' => 'teaser',
      'rss' => 0,
      'search_index' => 0,
      'search_result' => 0,
      'diff_standard' => 0,
      'node_gallery_node_thumbnail' => 0,
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => 0,
    'i18n' => 0,
    'module' => 'directory',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  // Exported flag: "I&#039;m the owner for registered".
  $flags['im_owner_for_registered'] = array(
    'entity_type' => 'node',
    'title' => 'I\'m the owner for registered',
    'global' => 0,
    'types' => array(
      0 => 'directory_entry',
    ),
    'flag_short' => 'I\'m the Owner of this product/service',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Ownership claimed',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => 'Ownership claimed',
    'link_type' => 'confirm',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 0,
      'teaser' => 0,
      'rss' => 0,
      'search_index' => 0,
      'search_result' => 0,
      'diff_standard' => 0,
      'node_gallery_node_thumbnail' => 0,
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => 'others',
    'show_contextual_link' => 0,
    'i18n' => 0,
    'flag_confirmation' => 'Are you sure? Give details below.',
    'unflag_confirmation' => 'Are you sure you want to un-flag this content?',
    'module' => 'directory',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  return $flags;

}

/**
 * Implements hook_image_default_styles().
 */
function directory_image_default_styles() {
  $styles = array();

  // Exported image style: 40x.
  $styles['40x'] = array(
    'name' => '40x',
    'label' => '30x',
    'effects' => array(
      1 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => '',
          'height' => 80,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function directory_node_info() {
  $items = array(
    'directory_entry' => array(
      'name' => t('Ginger Directory'),
      'base' => 'node_content',
      'description' => t('A form for business owners to promote their product/service'),
      'has_title' => '1',
      'title_label' => t('Name of business'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
