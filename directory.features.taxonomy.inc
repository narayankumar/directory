<?php
/**
 * @file
 * directory.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function directory_taxonomy_default_vocabularies() {
  return array(
    'directory_category' => array(
      'name' => 'Directory category',
      'machine_name' => 'directory_category',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
